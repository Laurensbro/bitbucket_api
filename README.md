# Bitbucket-api demo #



### What is this repository for? ###

* Authenticate, 
* consume your API, 
* get the commits, 
* search with XPath through JQuery


### How do I get set up? ###

* Clone
* Change "ValuesController.cs" to use your API-url and your login/pwd
* Run, goto "/index.html"
* Experiment with custom XPath in the textbox
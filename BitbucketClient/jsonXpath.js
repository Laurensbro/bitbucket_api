﻿var gitData = "";
$(document).ready(function () {

    $.get("http://localhost:2142/api/values", function (data) {
        gitData = data;

        //1. Get all authors
        var authors = JSON.search(data, '//*/author');

        var uniqueAuthors = [];
        for (var i = 0; i < authors.length; i++) {
            var found = false;
            for (var u = 0; u < uniqueAuthors.length; u++) {
                if (authors[i].user.username === uniqueAuthors[u].user.username) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                uniqueAuthors.push(authors[i]);
            }

        }

        for (let a = 0; a < uniqueAuthors.length; a++) {
            console.log(uniqueAuthors[a].user.username);
        }

        //2. Print all unique authors + avatar
        var usersDiv = $("div#divCommitters");
        for (let a = 0; a < uniqueAuthors.length; a++) {
            var userDivStr = "<div><img src='" + uniqueAuthors[a].user.links.avatar.href + "'/>" + uniqueAuthors[a].user.username + "</div>";
            usersDiv.append($(userDivStr));
        }
    });
});

var searchXpath = function () {
    var xpathstr = $("input#txtXpath").val();

    $("#divResult").html("");
    var xpathResult = JSON.search(gitData, xpathstr);
    for (var i = 0; i < xpathResult.length; i++) {
        $("#divResult").append(JSON.stringify(xpathResult[i]));
    }
}

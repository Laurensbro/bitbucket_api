﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace BitbucketClient.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public HttpResponseMessage Get()
        {
            var initialUrlstring = "yourbitbutcket-url";

            string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes("username" + ":" + "pwd"));

            int nrNewPages = 1;
            var nextUrl = initialUrlstring;
            List<dynamic> valuesList = new List<dynamic>();
            while (nrNewPages > 0)
            {
                var getCommitRequest = WebRequest.Create(nextUrl);
                getCommitRequest.Headers.Add("Authorization", "Basic " + credentials);

                using (var bbResponse = getCommitRequest.GetResponse() as HttpWebResponse)
                {
                    var reader = new StreamReader(bbResponse.GetResponseStream());

                    dynamic convert = JsonConvert.DeserializeObject(reader.ReadToEnd());
                    var values = convert.values;
                    valuesList.AddRange(values);
                    if (convert.next == null) break;
                    nextUrl = convert.next.Value;
                    nrNewPages--;
                }
            }

            var valuesToStringIsJson = JsonConvert.SerializeObject(valuesList);
            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(valuesToStringIsJson, System.Text.Encoding.UTF8, "application/json");
            return response;
        }
    }
}
